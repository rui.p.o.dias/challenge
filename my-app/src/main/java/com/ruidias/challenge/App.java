package com.ruidias.challenge;
import java.util.Arrays;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class App 
{
    /**
     * main class for the challenge program
     * matrix expected from stdin:
     * m n
     * a b c ...
     * d e f
     * g h i
     * ...
     * 
     */
    public static void main( String[] args )
    {
        int[][] matrix = new int[1][1];
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String l;
        try{
            l = br.readLine();
            String[] split_numbers = l.split("\\s+");
            int x = Integer.parseInt(split_numbers[0]);
            int y = Integer.parseInt(split_numbers[1]);//catch errors
            int[] tmp;
            matrix = new int[x][y];
            int idx = 0;
            while(true){
                l = br.readLine();
                if(l==null) break;
                split_numbers = l.split("\\s+");
                tmp = Arrays.asList(split_numbers).stream().mapToInt(Integer::parseInt).toArray();
                matrix[idx++]=tmp;
            }
        }catch(IOException e)
        {
            
            System.exit(1);
        }
        catch(NumberFormatException e){
            
            System.exit(1);
        }
    
        matrixPrinter(matrix);
        Solution s = new Solution(matrix);
        s.solve();
        ArrayList<Integer> solution = s.getSolution();
        System.out.println("Solution:");
        System.out.println(solution.toString());
    }

    /**
     * Simple matrix printer
     * @param m int[][] matrix to print
     */
    private static void matrixPrinter(int[][] m){

        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                System.out.print(m[i][j] + " ");
            }
            System.out.println();
        }
    }
}
