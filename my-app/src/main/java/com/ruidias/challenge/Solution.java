package com.ruidias.challenge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Solution class
 */
public class Solution {
    private int[][] m; /**Contains the matrix */
    private ArrayList<Integer> longestSequence; /**Contains the ArrayList with the integer values forming the longest sequence after solve() */
    private ArrayList<ArrayList<Integer>> sortedValues; /**Contains the matrix elements in an ArrayList<Integer> {value, row, column}. If sort() has been called, they sorted by value in ascending order */
    /**
     * Solution constructor
     * @param matrix int[][]
     */
    public Solution(int[][] matrix){
        m=matrix;
        sortedValues = new ArrayList<ArrayList<Integer>>();
        for(int y=0;y<m.length;y++){
            for(int x=0;x<m[0].length;x++){
                sortedValues.add(
                    new ArrayList<Integer>(Arrays.asList(m[y][x], y, x))
                );
            }

        }
    }

    /**
     * Solution solver algorithm
     */
    public void solve(){
        sortedValues = sort(sortedValues);

        if(sortedValues.size()==1){
            //In a 1x1 matrix, the solution is the only value
            longestSequence = new ArrayList<Integer>(Arrays.asList(sortedValues.get(0).get(0)));
            return;
        } 

        Iterator<ArrayList<Integer>> iter = sortedValues.iterator();
        int maxSize = 0;
        int currentMaxSize = 1;
        ArrayList<Integer> elem = iter.next();
        ArrayList<Integer> prevElem;
        ArrayList<Integer> currentSeq = new ArrayList<Integer>(Arrays.asList(elem.get(0)));
        ArrayList<Integer> largestSeq = new ArrayList<Integer>(currentSeq);
        
        while(iter.hasNext()){
            prevElem = elem;
            elem = iter.next();
            
            if(elem.get(0)==prevElem.get(0)+1){
                //is next in sequence number
                if(isAdjacent(elem,prevElem)){
                    //is adjacent
                    currentMaxSize +=1;
                    currentSeq.add(elem.get(0));
                    if(iter.hasNext())
                        continue; 
                }
            }
            //broke sequence
            if(currentMaxSize>maxSize){
                maxSize = currentMaxSize;
                largestSeq = new ArrayList<Integer>(currentSeq);
            }
            currentSeq = new ArrayList<Integer>(Arrays.asList(elem.get(0)));
            currentMaxSize = 1;
        }
        
       longestSequence=largestSeq;
    }

    /**
     * Method for calculating wether two elements in a M x N matrix are adjacent
     * @param e1 ArrayList<Integer> {value, row, col}
     * @param e2 ArrayList<Integer> {value, row, col}
     * @return true if the elements are adjacent, or false
     */
    public boolean isAdjacent(ArrayList<Integer> e1, ArrayList<Integer> e2){
        if(e1.get(1)==e2.get(1)){
            if(e1.get(2)==e2.get(2)+1 || e1.get(2)==e2.get(2)-1){
                return true;
            }
        }
        if(e1.get(2)==e2.get(2)){
            if(e1.get(1)==e2.get(1)+1 || e1.get(1)==e2.get(1)-1){
                return true;
            }
        }
        return false;
    }

    /**
     * Matrix getter
     * @return int[][] matrix
     */
    public int[][] getMatrix(){
        return m;
    }

    /**
     * Solution getter
     * @return ArrayList<Integer> with the longest sequence of numbers
     */
    public ArrayList<Integer> getSolution(){
        return longestSequence;
    }

    /**
     * Sort the objects by the matrix value in ascending order
     * @param toSort ArrayList<ArrayList<Integer>> to be sorted
     * @return ArrayList<ArrayList<Integer>> sorted in ascending order
     */
    public ArrayList<ArrayList<Integer>> sort(ArrayList<ArrayList<Integer>> toSort){
         toSort.sort(new myComparator());
         return toSort;
    }
}
/**
 * Auxiliary comparator
 */
class myComparator implements Comparator<ArrayList<Integer>>{
    /**
     * Compares the first object of an ArrayList with the first object of another ArrayList
     * @param obj1 first ArrayList
     * @param obj2 second ArrayList
     */
    @Override
    public int compare(ArrayList<Integer> obj1, ArrayList<Integer> obj2){
        return obj1.get(0).compareTo(obj2.get(0));
    }
}

