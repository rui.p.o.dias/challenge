package com.ruidias.challenge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Arrays;
import java.util.ArrayList;
import org.junit.Test;

public class SolutionTest {
    
    @Test
    public void testIsAdjacent() throws Throwable{
        //given
        int[][] m = {{0,9,1},{7,5,2},{8,4,3}};
        Solution s = new Solution(m);

        //when
        ArrayList<Integer> e1 = new ArrayList<Integer>(Arrays.asList(1,0,0));
        ArrayList<Integer> e2 = new ArrayList<Integer>(Arrays.asList(1,0,1));
        ArrayList<Integer> e3 = new ArrayList<Integer>(Arrays.asList(1,0,2));
        ArrayList<Integer> e4 = new ArrayList<Integer>(Arrays.asList(1,1,1));
        ArrayList<Integer> e5 = new ArrayList<Integer>(Arrays.asList(1,1,0));
        ArrayList<Integer> e6 = new ArrayList<Integer>(Arrays.asList(1,2,0));
        ArrayList<Integer> e7 = new ArrayList<Integer>(Arrays.asList(1,4,4));
        ArrayList<Integer> e8 = new ArrayList<Integer>(Arrays.asList(1,3,3));

        //then
        assertEquals(true, s.isAdjacent(e1, e2));
        assertEquals(false, s.isAdjacent(e1, e3));
        assertEquals(true, s.isAdjacent(e2, e3));
        assertEquals(true, s.isAdjacent(e4, e2));
        assertEquals(true, s.isAdjacent(e5, e6));
        assertEquals(false, s.isAdjacent(e6, e7));
        assertEquals(false, s.isAdjacent(e7, e8));

    }

    @Test
    public void testSort() throws Throwable{
        int[][] m = {{0,9,1},{7,5,2},{8,4,3}};
        Solution s = new Solution(m);

        ArrayList<ArrayList<Integer>> sv1 = new ArrayList<ArrayList<Integer>>();
        sv1.add(new ArrayList<Integer>(Arrays.asList(0, 0, 0)));
        sv1.add(new ArrayList<Integer>(Arrays.asList(1, 0, 2)));
        sv1.add(new ArrayList<Integer>(Arrays.asList(2, 1, 2)));
        sv1.add(new ArrayList<Integer>(Arrays.asList(3, 2, 2)));
        sv1.add(new ArrayList<Integer>(Arrays.asList(4, 2, 1)));
        sv1.add(new ArrayList<Integer>(Arrays.asList(5, 1, 1)));
        sv1.add(new ArrayList<Integer>(Arrays.asList(7, 1, 0)));
        sv1.add(new ArrayList<Integer>(Arrays.asList(8, 2, 0)));
        sv1.add(new ArrayList<Integer>(Arrays.asList(9, 0, 1)));

        ArrayList<ArrayList<Integer>> sv2 = new ArrayList<ArrayList<Integer>>();
        
        sv2.add(new ArrayList<Integer>(Arrays.asList(3, 2, 2)));
        sv2.add(new ArrayList<Integer>(Arrays.asList(4, 2, 1)));
        sv2.add(new ArrayList<Integer>(Arrays.asList(8, 2, 0)));
        sv2.add(new ArrayList<Integer>(Arrays.asList(9, 0, 1)));
        sv2.add(new ArrayList<Integer>(Arrays.asList(5, 1, 1)));
        sv2.add(new ArrayList<Integer>(Arrays.asList(7, 1, 0)));
        sv2.add(new ArrayList<Integer>(Arrays.asList(1, 0, 2)));
        sv2.add(new ArrayList<Integer>(Arrays.asList(2, 1, 2)));
        sv2.add(new ArrayList<Integer>(Arrays.asList(0, 0, 0)));
        
        assertNotEquals(sv1, sv2);
        assertEquals(sv1, s.sort(sv2));
    }

    @Test
    public void testSolution() throws Throwable{
        int[][] m = {{0,9,1},{7,5,2},{8,4,3}};
        ArrayList<Integer> expectedSolution = new ArrayList<Integer>(Arrays.asList(1,2,3,4,5));
        Solution s = new Solution(m);
        s.solve();
        ArrayList<Integer> testSolution = s.getSolution();

        assertEquals(expectedSolution, testSolution);
    }

}
